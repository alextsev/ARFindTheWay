﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu_Handler : MonoBehaviour
{
    public GameObject SelectButton;
    public GameObject NFCButton;
    public GameObject ControlButton;
    public GameObject AbouttButton;
    public GameObject ExittButton;
    public GameObject DropDownButton;
    public GameObject StarttButton;
    public GameObject BackBButton;
    public GameObject BackBButton2;

    public Text label;
    public Text upotitle;
    public Text about;
    public Text controls;

    void Start ()
    {
        DropDownButton.SetActive(false);
        StarttButton.SetActive(false);
        BackBButton.SetActive(false);
        BackBButton2.SetActive(false);
        about.gameObject.SetActive(false);
        controls.gameObject.SetActive(false);    
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
            }
        }
    }
    public void StartButton()
    {
        if(label.text == "Building 5th Floor")
        { 
            SceneManager.LoadScene(1);
        }
        else if (label.text == "Option B")
        {
            SceneManager.LoadScene(0);
        }
        else if (label.text == "Option C")
        {
            SceneManager.LoadScene(1);
        }
    }
    public void SelectBtn()
    {
        upotitle.text = "Select Building :";
        ShortcutDisableButtons();
        DropDownButton.SetActive(true);
        StarttButton.SetActive(true);
        BackBButton.SetActive(true);
    }
    public void NFCBtn()
    {
        SceneManager.LoadScene(2);
    }
    public void ControlsButton()
    {
        upotitle.text = "Controls :";
        controls.gameObject.SetActive(true);
        ShortcutDisableButtons();
        BackBButton.SetActive(false);
        BackBButton2.SetActive(true);
    }
    public void AboutButton()
    {
        upotitle.text = "About the App :";
        about.gameObject.SetActive(true);
        ShortcutDisableButtons();
        BackBButton.SetActive(true);
    }
    public void BackButton()
    {
        upotitle.text = "Menu :";
        ShortcutEnableButtons();
        DropDownButton.SetActive(false);
        StarttButton.SetActive(false);
        BackBButton.SetActive(false);
        about.gameObject.SetActive(false);
    }
    public void BackButton2()
    {
        upotitle.text = "Menu :";
        ShortcutEnableButtons();
        controls.gameObject.SetActive(false);
        BackBButton2.SetActive(false);
    }
    public void ExitButton()
    {
        Application.Quit();
    }

    private void ShortcutEnableButtons()
    {
        SelectButton.SetActive(true);
        NFCButton.SetActive(true);
        ControlButton.SetActive(true);
        AbouttButton.SetActive(true);
        ExittButton.SetActive(true);
    }
    private void ShortcutDisableButtons()
    {
        SelectButton.SetActive(false);
        NFCButton.SetActive(false);
        ControlButton.SetActive(false);
        AbouttButton.SetActive(false);
        ExittButton.SetActive(false);
    }
}
